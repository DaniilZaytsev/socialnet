import React from "react";
import "./App.css";
import Header from "./Components/Header/Header";
import Navbar from "./Components/NavBar/Navbar";
import Profile from "./Components/Profile/Profile";
import Dialogs from "./Components/Dialogs/Dialogs";
import News from "./Components/News/News";
import Music from "./Components/Music/Music"
import Settings from "./Components/Settings/Settings";
import { BrowserRouter, Route } from "react-router-dom";

const App = (props) => {
  
  return ( 
    
    <BrowserRouter>
    <div className="app-wrapper">
      <Header />
      <Navbar/>
      {/* <Profile/> */}
          <div className="app-wrapper-content">
              <Route path='/dialogs' render={ () => <Dialogs DialogsData={props.DialogsData} MessageData={props.MessageData}/>}/>
              <Route path="/profile"render={ () => <Profile postData={props.postData}/>}/>
              
              <Route path="/news"component={News}/>
              <Route path="/music"component={Music}/>
              <Route path="/settings" component={Settings}/>
          </div>
    </div>
    </BrowserRouter>
  );
}; 
  
export default App;
