import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

let DialogsData =
  [
      {id:'1', name:'Olya'},
      {id:'2', name:'Danya'},
      {id:'3', name:'Missik'},
      {id:'4', name:'Julik'},
      {id:'5', name:'Cruella'}
  ];

let postData=[
  {id:'1', message:'Hi bitch', likesCount:15},
    {id:'2', message:'My first post', likesCount:16}
  ]
  let MessageData=[
    {id :'1', message:'I love u'},
    {id :'2', message:'I lov u 2'},
    {id :'3', message:'MRRRR'},
    {id :'4', message:'GAvgav'},
    {id :'5', message:'Friday mood'},
  ]

ReactDOM.render(
  <React.StrictMode>
    <App postData={postData} DialogsData={DialogsData} MessageData={MessageData}/>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
