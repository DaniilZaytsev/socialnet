import React from "react";
import { NavLink } from "react-router-dom";
import s from "./Dialogs.module.css";
import DialogItem from "./DialogItem/DialogItem";
import Message from "./Message/Message";


const Dialogs = (props) => {
  

let dialogsElements= props.DialogsData.map(dialog=><DialogItem name={dialog.name} id= {dialog.id}/>);
let messageElements= props.MessageData.map(m=><Message message={m.message} id={m.id}/>);

  return (
    <div className={s.Dialogs}>
      <div className={s.DialogsItems}>
        {dialogsElements}
      </div>
      <div className={s.messages}>
        {messageElements}
      </div>
    </div>
  );
}; 

export default Dialogs;
