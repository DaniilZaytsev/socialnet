import React from "react";
import { NavLink } from "react-router-dom";
import s from "./../Dialogs.module.css";

const Message=({message})=> <div className={s.message}>{message}</div>


export default Message;