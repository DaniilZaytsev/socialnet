import React from "react";
import s from "./ProfileInfo.module.css";

const ProfileInfo = () => {
  return (
    <div className={s.ProfileInfo}>
      <div>
        <img src="https://avatarko.ru/img/kartinka/1/Crazy_Frog.jpg"></img>
      </div>
      <div className={s.description}> ava + description</div>
    </div>
  );
};
export default ProfileInfo;