import React from "react";
import s from'./Header.module.css';

const Header = () => {
  return (
    <header className={s.header}>
      <img src="https://image.flaticon.com/icons/png/512/2175/2175282.png"></img>
    </header>
  );
};

export default Header;